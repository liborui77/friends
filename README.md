# friends

#### 介绍
Steve Li's Blog的友链

#### Usage
大佬们，加个友链呗~~~

要求：
1. 有独立自主的域名（不要使用github gitee gitlab coding 的分配域名）
2. 全站HTTPS
3. 有本站域名
4. 墙内可访问

满足以上要求后

在本项目开一个issues

格式如下:
```
**标题请书写你的链接，不要写其他内容**
```yaml
# 显示名称
name: 你的名称

# 跳转地址
link: 你的链接

# 你的头像
avatar: 你的头像

# 你的描述
descr: 你的描述

#------------------------------#
#       以下字段为选填字段       #

# 边框及鼠标悬停的背景颜色，允许设置渐变色
--primary-color: #49b1f5

# 边框大小
border-width: 0px

# 边框样式
border-style: solid

# 鼠标悬停头像旋转角度
--primary-rotate: 0deg

# 边框动画 参考 https://developer.mozilla.org/zh-CN/docs/Web/CSS/animation
# 内置动画：borderFlash（边框闪现）、link_custom1(跑马灯)、link_custom(主颜色呼吸灯)
animation: borderFlash 0s infinite alternate

# 头像动画 参考 https://developer.mozilla.org/zh-CN/docs/Web/CSS/animation
# 内置动画：auto_rotate_left（左旋转）、auto_rotate_right（右旋转）
img_animation: auto_rotate_right 0s linear infinite

# 风格 可选项 item和card
card_style: item
# 自定义网站截图（当样式为card时可以自定义网站截图，防止api接口宕掉无法显示图）
screenshot: 
```

```
然后等待审核
周一到周五在学校关着，可能回复不及时

